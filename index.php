<?php

require __DIR__ . '/vendor/autoload.php';

use App\Controller\HomeOwnerController;


// let's imagine this works as a route
echo json_encode((new HomeOwnerController())->main());

