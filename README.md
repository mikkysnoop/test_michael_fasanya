* A test exercise written by Michael Fasanya

* git clone https://gitlab.com/mikkysnoop/test_michael_fasanya/-/tree/main

* Navigate to project folder

* run composer install

* Install php (https://www.sitepoint.com/how-to-install-php-on-windows) 

* Start a server  (php -S localhost:8080)

* You can run the test file @app\test (php vendor/phpunit/phpunit/phpunit testPersonServices.php)