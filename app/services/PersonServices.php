<?php

declare(strict_types=1);

namespace App\services;

use App\dto\PersonsDto;
use App\interfaces\PersonsDtoInterface;

class PersonServices
{
    private PersonsDtoInterface $personsDto;

    private const COUPLES = 'and';

    /**
     * @param PersonsDtoInterface $personsDto
     */
    public function __construct(
        PersonsDtoInterface $personsDto,
    ) {
        $this->personsDto = $personsDto;
    }

    /**
     * @param $fileFirstIterable
     * @return array
     */
    public function isCouples($fileFirstIterable): array
    {
        $data = explode(self::COUPLES, $fileFirstIterable);
        count($data) == 1 ? $data = explode('&', $fileFirstIterable) : null;
        $map = explode(' ', end($data));

        $owners = [];

        foreach ($data as $key => $details) {
            $couples = $this->personsDto;
            if ($key == 0) {
                $couples->title = $details;
            } else {
                $couples->title = $map[1];
            };
            $couples->first_name = null;
            $couples->initial = null;
            $couples->last_name = $map[2];

            $owners[] = (array)$couples;
        }

        return $owners;
    }


    /**
     * @param string $fileFirstIterable
     * @return PersonsDto
     */
    public function isSingleOwner(string $fileFirstIterable): PersonsDto
    {
        $data = explode(' ', $fileFirstIterable);

        $singleOwner = $this->personsDto;
        $singleOwner->title = $data[0];
        $singleOwner->first_name = !$this->isInitial($data[1]) ? $data[1] : null;
        $singleOwner->initial = $this->isInitial($data[1]) ? $data[1] : null;
        $singleOwner->last_name = end($data);

        return $singleOwner;
    }

    /**
     * @param string $data
     * @return bool
     */
    private function isInitial(string $data): bool
    {
          $initial = strlen($data);
          return str_contains($data, '.') || $initial == 1;
    }

    /**
     * @param string $fileFirstIterable
     * @return PersonsDto | array
     */
    public function formatHomeOwnerData(string $fileFirstIterable): PersonsDto|array
    {
        $couplesOwner = str_contains($fileFirstIterable, self::COUPLES) || str_contains($fileFirstIterable, '&');

        if ($couplesOwner) {
            $ownerData = $this->isCouples($fileFirstIterable);
        } else {
            $ownerData = $this->isSingleOwner($fileFirstIterable);
        }

        return $ownerData;
    }

    /**
     * @param array $csvDataList
     * @return array
     */
    public function getOwners(array $csvDataList): array
    {
        $homeOwner = [];

        foreach ($csvDataList as $csvFileData) {
            $homeOwner[] = (array)$this->formatHomeOwnerData($csvFileData);
        }

        return $homeOwner;
    }
}
