<?php

namespace App\dto;

use App\interfaces\PersonsDtoInterface;

class PersonsDto implements PersonsDtoInterface
{
    public ?string $title;
    public ?string $first_name;
    public ?string $initial;
    public ?string $last_name;


    public function __construct()
    {
        $this->title = null;
        $this->first_name = null;
        $this->initial = null;
        $this->last_name = null;
    }
}
