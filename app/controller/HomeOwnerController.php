<?php

namespace App\controller;

use App\services\PersonServices;
use App\dto\PersonsDto;

class HomeOwnerController
{
    /**
     * @return PersonsDto|array
     */
    public function main(): array|PersonsDto
    {
        /*
           * Get data coming from file upload request
             $file = $request['file];

           * Convert the $request file to php array, this will be done via services,
             but for the purpose of this test lets leave it here
             $csvFileData = str_getcsv($file);
        */

        // sample converted csv array, You can tinker with this array
        $csvFileData = [
            'Mr and Mrs Smith',
            'Mrs Jane Faith',
            'Mr F. Fredrickson'
        ];

        $personServices = new PersonServices(
            new PersonsDto(),
        );

        return $personServices->getOwners($csvFileData);
    }
}
