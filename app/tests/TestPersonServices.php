<?php

declare(strict_types=1);

namespace App\test;

use App\dto\PersonsDto;
use App\services\PersonServices;
use PHPUnit\Framework\TestCase;

//@test
class TestPersonServices extends TestCase
{
    public function testThatItReturnsExactName()
    {
        // given
        $fileFirstIterable = [
           'Mr John Smith'
        ];

        //when
        $personServices = new PersonServices(
            new PersonsDto(),
        );

        //then
        $this->assertEquals(
        //todo use snapshots
            '[{"title":"Mr","first_name":"John","initial":null,"last_name":"Smith"}]',
            json_encode($personServices->getOwners($fileFirstIterable))
        );
    }

    public function testThatItReturnsInitialWithDot()
    {
        // given
        $fileFirstIterable = [
            'Mr J. Smith'
        ];

        //when
        $personServices = new PersonServices(
            new PersonsDto(),
        );

        //then
        $this->assertEquals(
        //todo use snapshots
            '[{"title":"Mr","first_name":null,"initial":"J.","last_name":"Smith"}]',
            json_encode($personServices->getOwners($fileFirstIterable))
        );
    }

    public function testThatItReturnsInitialWithoutDot()
    {
        // given
        $fileFirstIterable = [
            'Mr H Smith'
        ];

        //when
        $personServices = new PersonServices(
            new PersonsDto(),
        );

        //then
        $this->assertEquals(
        //todo use snapshots
            '[{"title":"Mr","first_name":null,"initial":"H","last_name":"Smith"}]',
            json_encode($personServices->getOwners($fileFirstIterable))
        );
    }

    public function testThatItReturnsCouples()
    {
        // given
        $fileFirstIterable = [
            'Mr and Mrs Smith'
        ];

        //when
        $personServices = new PersonServices(
            new PersonsDto(),
        );

        //then
        $this->assertEquals(
        //todo use snapshots
            '[[{"title":"Mr ","first_name":null,"initial":null,"last_name":"Smith"},' .
            '{"title":"Mrs","first_name":null,"initial":null,"last_name":"Smith"}]]',
            json_encode($personServices->getOwners($fileFirstIterable))
        );
    }
}
